#!/usr/bin/python
import requests
import time
from bs4 import BeautifulSoup
from elasticsearch import Elasticsearch
from datetime import datetime
import ssl
import random
import shutil
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime	.text import MIMEText
import urllib3
import hashlib
from urlparse import urlparse
urllib3.disable_warnings()
es = Elasticsearch()
datetime_str = datetime.now().strftime("%Y-%m-%d %H:%M")
datetime_file = datetime.now().strftime("%Y-%m-%d_%H-%M")
filename='./log/newsengine_'+datetime_file+'.log'
elasticsearchMailIndex="news"
class newsEngine():
	def __init__(self):
		###START SETTINGS###
		self.Flag=1
		self.Retry=3
		self.elasticsearchIndex="news"
		###END SETTINGS###
		with open(filename, 'a') as f:
			f.write(datetime_str+"  ---Starting--- \n")
			f.write(datetime_str+"  Flag - "+str(self.Flag)+" \n")
			f.write(datetime_str+"  Retry - "+str(self.Retry)+" \n")
			f.write(datetime_str+"  LogFile - "+filename+" \n")
		
	def requestSend(self,url):
		rand1=random.randint(1, 30)
		rand2=random.randint(1, 30)
		user_agent = {'User-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.14'+str(rand1)+'.90 Safari/537.'+str(rand2)+''}
		for j in range(1,self.Retry+1):
			try:
				reqv=requests.get(url, headers = user_agent)
				break
			except:
				with open(filename, 'a') as f:
					f.write(datetime_str+"  "+url+"  - Request Failed Retry..."+str(j)+" \n")
				if self.Retry==j:
					with open(filename, 'a') as f:
						f.write(datetime_str+"  "+url+" - Retry Failed Skipping \n")
					return
		return reqv
	###START YCOMBINATOR FUNCTION###
	def ycombinator(self):
		i=1
		data_mail=[]
		while True:
			url="https://news.ycombinator.com/news?p=%d"%i
			reqv=self.requestSend(url)
			reqtextv=reqv.text
			soup = BeautifulSoup(reqtextv, 'html.parser')
			j=1
			for z3,z2 in zip(soup.select(".storylink"),soup.select(".athing")):
				try:
					es.get(index=self.elasticsearchIndex, doc_type="ycombinator", id=z2['id'])
				except:
					url_c=z3['href']
					url_p=urlparse(z3['href'])
					if url_p.netloc =='':
						url_c='news.ycombinator.com/'+z3["href"]
					if url_p.scheme =='':
						url_c='https://'+url_c
					z3['href']=url_c
					data_mail.append({'title':z3.text,'link':z3['href'],'id':z2['id']})
				j+=1
			if j<30:##this is logic as ycombinator shows only 30 results on one page, so we can break if it is reached on last page
				break
			else:
				i+=1
		if len(data_mail)>0:
			for j2 in reversed(data_mail):
				try:
					doc = {'unix_time':int(round(time.time())),'title':j2["title"],'link':j2["link"],'timestamp': datetime.now(),}
					if self.Flag==1:
						res = es.index(index=self.elasticsearchIndex, doc_type='ycombinator',id=j2["id"], body=doc)
					else: 
						with open(filename, 'a') as f:
							f.write(datetime_str+" ycombinator - saving is Off fetching..."+j2["link"]+"\n")
				except:
					with open(filename, 'a') as f:
						f.write(datetime_str+" ycombinator - Elastic Save failed! Link - '"+j2["link"]+"' \n")
				time.sleep(0.01)
				
		'''Start ycombinator data join'''
		html=''
		counter=1
		for d in data_mail:
			html=html+"""<span class="listspan">"""+str(counter)+""". <a href='"""+d['link'].encode('ascii','ignore')+"""' target='_blank'>"""+d['title'].encode('ascii','ignore')+"""</a></span><br> """
			counter+=1
		if len(html)>0:
			email_content_ycombinator="""<h3>Ycombinator News</h3>"""+html
		else:
			email_content_ycombinator=''
		return email_content_ycombinator
	###END YCOMBINATOR FUNCTION###

	###START TIMESOFINDIA FUNCTION###
	def timesofindia(self):
		data_mail_timesofindia_top=[]
		url="http://timesofindia.indiatimes.com"
		reqv=self.requestSend(url)
		reqtextv=reqv.text
		soup = BeautifulSoup(reqtextv, 'html.parser')
		for z1 in soup.select(".top-story a"):
			if len(z1.text)>0:
				url_c=z1['href']
				url_p=urlparse(z1['href'])
				if url_p.netloc =='':
					url_c='timesofindia.indiatimes.com'+z1["href"]
				if url_p.scheme =='':
					url_c='http://'+url_c
				z1['href']=url_c
				uniq_md5=hashlib.md5(z1['href']).hexdigest()
				try:
					es.get(index=self.elasticsearchIndex, doc_type="timesofindia", id=uniq_md5)
				except:
					data_mail_timesofindia_top.append({'title':z1.text,'link':z1['href'],'id':uniq_md5})
					doc = {'unix_time':int(round(time.time())),'title':z1.text,'link':z1['href'],'type':'top','timestamp': datetime.now(),}
					try:
						if self.Flag==1:
							res = es.index(index=self.elasticsearchIndex, doc_type='timesofindia',id=uniq_md5, body=doc)
						else: 
							with open(filename, 'a') as f:
								f.write(datetime_str+" timesofindia (top-story) - saving is Off fetching..."+z1['href']+"\n")
					except:				
						with open(filename, 'a') as f:
							f.write(datetime_str+" timesofindia (top-story) - Elastic Save failed! Link - "+z1['href']+" \n")
						time.sleep(0.01)
		data_mail_timesofindia_latest=[]
		for z1 in soup.select("#lateststories a"):
			if len(z1.text)>0:
				url_c=z1['href']
				url_p=urlparse(z1['href'])
				if url_p.netloc =='':
					url_c='timesofindia.indiatimes.com'+z1["href"]
				if url_p.scheme =='':
					url_c='http://'+url_c
				z1['href']=url_c
				uniq_md5=hashlib.md5(z1['href']).hexdigest()
				try:
					es.get(index=self.elasticsearchIndex, doc_type="timesofindia", id=uniq_md5)
				except:
					data_mail_timesofindia_latest.append({'title':z1.text,'link':z1['href'],'id':uniq_md5})
					doc = {'unix_time':int(round(time.time())),'title':z1.text,'link':z1['href'],'type':'latest','timestamp': datetime.now(),}
					try:
						if self.Flag==1:
							res = es.index(index=self.elasticsearchIndex, doc_type='timesofindia',id=uniq_md5, body=doc)
						else:
							with open(filename, 'a') as the_file:
								the_file.write(datetime_str+" timesofindia (latest) - saving is Off fetching..."+z1['href']+"\n")
					except:
						with open(filename, 'a') as the_file:
							the_file.write(datetime_str+" timesofindia (latest) - Elastic Save failed! Link - "+z1['href']+" \n")
						time.sleep(0.01)
		'''Start timesofindia data join'''
		html=''	
		counter=1
		for d1 in data_mail_timesofindia_top:
			html=html+"""<span class="listspan">"""+str(counter)+""". <a href='"""+d1['link'].encode('ascii','ignore')+"""' target='_blank'>"""+d1['title'].encode('ascii','ignore')+"""</a></span><br> """
			counter+=1
		if len(html)>0:
			email_content_timesofindia_top="""<h3>Top News Stories From Times Of India</h3>"""+html
		else:
			email_content_timesofindia_top=''
		html=''	
		counter=1
		for d1 in data_mail_timesofindia_latest:
			html=html+"""<span class="listspan">"""+str(counter)+""". <a href='"""+d1['link'].encode('ascii','ignore')+"""' target='_blank'>"""+d1['title'].encode('ascii','ignore')+"""</a></span><br> """
			counter+=1
		if len(html)>0:
			email_content_timesofindia_latest="""<h3>Latest News Stories From Times Of India</h3>"""+html
		else:
			email_content_timesofindia_latest=''
		timesDataCombined=email_content_timesofindia_top+email_content_timesofindia_latest
		return timesDataCombined
	###END TIMESOFINDIA FUNCTION###

	###START NYTIMES FUNCTION###
	def nytimes(self):
		url="http://www.nytimes.com"
		reqv=self.requestSend(url)
		reqtextv=reqv.text
		soup = BeautifulSoup(reqtextv, 'html.parser')
		data_mail_nytimes=[]
		for z2 in soup.select(".story-heading a"):
			if len(z2.text)>0:
				uniq_md5=hashlib.md5(z2['href']).hexdigest()
				try:
					es.get(index=self.elasticsearchIndex, doc_type="nytimes", id=uniq_md5)
				except:
					data_mail_nytimes.append({'title':z2.text,'link':z2['href'],'id':uniq_md5})
					doc = {'unix_time':int(round(time.time())),'title':z2.text,'link':z2['href'],'timestamp': datetime.now(),}
					try:
						if self.Flag==1:
							res = es.index(index=self.elasticsearchIndex, doc_type='nytimes',id=uniq_md5, body=doc)
						else: 
							with open(filename, 'a') as the_file:
								the_file.write(datetime_str+" nytimes - saving is Off fetching..."+z2['href']+ "\n")
					except:
						with open(filename, 'a') as the_file:
							the_file.write(datetime_str+" nytimes - Elastic Save failed! Link - "+z2['href']+ "\n")
					time.sleep(0.01)
		'''Start nytimes data join'''
		html=''	
		counter=1
		for d1 in data_mail_nytimes:
			html=html+"""<span class="listspan">"""+str(counter)+""". <a href='"""+d1['link'].encode('ascii','ignore')+"""' target='_blank'>"""+d1['title'].encode('ascii','ignore')+"""</a></span><br> """
			counter+=1
		if len(html)>0:
			email_content_nytimes="""<h3>Top News Stories From New York Times</h3>"""+html
		else:
			email_content_nytimes=''
		return email_content_nytimes
###END NYTIMES FUNCTION###
####CLASS END####

###START Send mail###
def py_mail(SUBJECT, BODY, TO, FROM):
	"""With this function we send out our html email"""
	MESSAGE = MIMEMultipart('alternative')
	MESSAGE['subject'] = SUBJECT
	MESSAGE['To'] = TO
	MESSAGE['From'] = FROM
	MESSAGE.preamble = """
Your mail reader does not support the report format.
Please visit us at <a href="http://shivalink.com/news.php">Shivalink.com</a> to read News!"""
	BODY1="""
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>html title</title>
<style type="text/css" media="screen">
	.listspan{
		background-color:#e3e3e3;
		line-height:24px;
	}
</style>
</head>
<body>
You can also read on <a href="http://shivalink.com/news.php">shivalink.com</a>.<br><br>
"""+BODY+"""
You can also read on <a href="http://shivalink.com/news.php">shivalink.com</a>.<br><br>
To unsubscribe Enter email on subscribe from on shivalink.com and do not click on activation link that you will receive in Email.
</body>
</html>
"""
	HTML_BODY = MIMEText(BODY1, 'html', _charset='iso-8859-1')
	MESSAGE.attach(HTML_BODY)
	server = smtplib.SMTP('localhost')
#	if __name__ == "__main__":
#		server.set_debuglevel(0)
 	server.set_debuglevel(False)
	password = "9719277016"
	server.starttls()
	server.login(FROM,password)
	server.sendmail(FROM, [TO], MESSAGE.as_string())
	server.quit()
###END Send mail###

###START EMAIL CODE###
FROM	="newsengine@shivalink.com"
SUBJECT	='knowledge & news updates for today'
newsObj=newsEngine()
news_source_dic={}
news_source_dic["ycombinator"]=newsObj.ycombinator()
news_source_dic["timesofindia"]=newsObj.timesofindia()
news_source_dic["nytimes"]=newsObj.nytimes()
query={"query" : {"match_all" : {}}}
data=es.search(index=elasticsearchMailIndex, doc_type="sub_email", body=query)
for  em in  data["hits"]["hits"]:
	if 'act' in em["_source"] and em["_source"]["act"]==1:
		TO=em["_id"]
		with open(filename, 'a') as f:
			f.write(datetime_str+"  "+TO+ "\n")
		email_content='Hi, '+em["_source"]["name"]	+' your news updates are here'
		for news_source in em["_source"]["news_source"]:
			email_content=email_content+news_source_dic[news_source]
			email_content=" ".join(email_content.split())
		py_mail(SUBJECT, email_content, TO, FROM)
with open(filename, 'a') as f:
	f.write(datetime_str+"  All Done now sleeping..."+ "\n")
###END EMAIL CODE###